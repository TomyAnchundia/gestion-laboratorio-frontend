import React, { useState, useEffect } from "react";
import "./css/materia.css";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "./Navbar";


const Materia = () => {
  //useState
  const [materia, setMateria] = useState({
    nombre_materia: ""
  });

  //defi9niendo el setLoading
  const [loading, setLoading] = useState(false);
  //definiendo el estado para editar el setLoading
  const [editing, setEditing] = useState(false);
  //definienfo el use navigate
  const navigate = useNavigate();
  const params = useParams();

  // eventos de la captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();
    //establecemos el loading
    setLoading(true);

    if (editing) {
      await fetch(`http://localhost:4000/materia/${params.id_materia}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(materia),
      });
    } else {
      await fetch("http://localhost:4000/materia", {
        method: "POST",
        body: JSON.stringify(materia),
        headers: { "Content-Type": "application/json" },
      });
    }

    // console.log(data);
    setLoading(false);

    if (editing) {
      alert("materia editada con exito !!!")
    } else {
      alert("materia creada con exito!!!")
    }

    navigate("/materia");
  };

  const handleChange = (e) => {
    setMateria({ ...materia, [e.target.name]: e.target.value });
  };

  const loadMateria = async (id_materia) => {
    const res = await fetch(`http://localhost:4000/materia/${id_materia}`);
    const data = await res.json();
    setMateria({ nombre_materia: data.nombre_materia });
    setEditing(true);
  };
  useEffect(() => {
    if (params.id_materia) {
      loadMateria(params.id_materia);
    }
  }, [params.id_materia]);

  const [errores, setErrores] = useState({});
  const validateForm = (materia) => {

    let errores = {};

    let nombre = /^[a-zA-ZÀ-ÿ\s0-9]{1,100}$/; // Letras y espacios, pueden llevar acentos.

    if (materia.nombre_materia === "") {
      errores.nombre_materia = "requiere completar el campo"
    } else if (nombre.test(materia.nombre_materia) === false) {
      errores.materia.nombre_materia = "solo letras y espacios, pueden llevar acentos y numeros"
    }

    return errores;
  };

  const handleBlur = (e) => {
    handleChange(e);
    setErrores(validateForm(materia))
  };

  return (


    <>
      <Navbar />
      <div className="container-materia">
        <h1> {editing ? "Editar Materia" : "Crear una nueva materia"}</h1>
        <form className="materia" onSubmit={handleSubmit}>
          <div className="date-materia">
            <input
              type="text"
              required
              onChange={handleChange}
              onBlur={handleBlur}
              name="nombre_materia"
              value={materia.nombre_materia}
            />
            <span></span>
            <label>Nombre de la materia</label>
          </div>
          {errores.nombre_materia && <p className='error'>{errores.nombre_materia}</p>}

          <input className="btn-guardar-materia" type="submit" value={editing ? "actualizar" : "crear"} />
        </form>
      </div>
    </>

  );
};

export default Materia;
